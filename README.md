# nginxConf
##########################################################



server {
    listen 80 default_server;
    listen [::]:80 default_server;
    root /var/www/html
    servername public ip add/hostname
    
    error_log /var/www/html/log/error.log
    
    location / {
        try_files $uri $uri.html $uri/ @extensionless-php;
        index index.html index.htm index.php;
    }

    location ~ \.php$ {
        include snippets/fastcgi-php.conf;
        # With php7.0-fpm:
        fastcgi_pass unix:/run/php/php7.2-fpm.sock;
    }
    
    location @extensionless-php {
        rewrite ^(.*)$ $1.php last;
    }
    
    location ~ /\.ht {
       deny all;
    }
}